﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$WorkDir = $here

$TargetRoot = $WorkDir | Join-Path -ChildPath "01-merged-text-file"
$OutRoot = $WorkDir | Join-Path -ChildPath "02-unmerged-text-files"

Push-Location -Path $WorkDir

Get-ChildItem -Path $TargetRoot -File |
?{ $_.Extension -Eq ".txt" } |
%{
    $Target = $_
    $OutDir = $OutRoot | Join-Path -ChildPath ($Target.BaseName)
    Write-Host -Object ("")
    Write-Host -Object ("Target is : " + $Target.FullName)
    
    
    $FileData =
    Get-Content -Path $Target.FullName -Raw -Encoding utf8 |
    ConvertFrom-Json
    
    foreach ($data in $FileData)
    {
        Write-Host -Object ([string]($data.RelPath))
        $OutFilePath = $OutDir | Join-Path -ChildPath ([string]($data.RelPath))
    
        $OutputFolderPath = Split-Path -Path $OutFilePath -Parent
        If (Test-Path -Path $OutputFolderPath -PathType Container) {
        } Else {
            New-Item -Path $OutputFolderPath -ItemType Directory | Out-Null
        }
    
        $data.Content | Out-File -FilePath $OutFilePath -Encoding utf8
    }
}

Pop-Location

Read-Host "Fin! Press Enter to exit..." | Out-Null
