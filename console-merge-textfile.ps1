﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$WorkDir = $here

$Target = $WorkDir | Join-Path -ChildPath "00-target-files"
$OutDir = $WorkDir | Join-Path -ChildPath "01-merged-text-file"

Push-Location -Path $WorkDir

If (Test-Path -Path $Target -PathType Container) {
} Else {
    New-Item -Path $Target -ItemType Directory | Out-Null
}
If (Test-Path -Path $OutDir -PathType Container) {
} Else {
    New-Item -Path $OutDir -ItemType Directory | Out-Null
}


Write-Host -Object "Input the path of the target folder."
Write-Host -Object "Or just hit enter to set the target to '00-target-files'."
$NewTarget = Read-Host -Prompt "Target"
If ($NewTarget -Ne "") { $Target = $NewTarget }

Write-Host -Object ("")
Write-Host -Object ("Target is : " + $Target)


$ExcludeExtensions = @(
    ".dll",
    ".xml",
    ".zip"
)

# $ExcludeFileName = @(
#     "merged.txt"
# )

$Contents =
Get-ChildItem -Path $Target -File -Recurse |
?{ $_.FullName -NotLike "*\.git\*" } |
?{ $ExcludeExtensions -notcontains $_.Extension } |
# ?{ $ExcludeFileName -notcontains $_.Name } |
%{
    $File = $_

    New-Object -TypeName PsObject -Property @{
        "RelPath" = $File.FullName.Replace($Target, "")
        "Content" = [string](Get-Content -Path $File.FullName -Raw -Encoding utf8)
    }
}


$OutFile = $OutDir | Join-Path -ChildPath ((Split-Path -Path $Target -Leaf) + ".txt")
Write-Host -Object ("")
Write-Host -Object ("MergedFile is : " + $OutFile)

$Contents |
ConvertTo-Json |
Out-File -FilePath $OutFile -Encoding utf8

Pop-Location

Read-Host "Fin! Press Enter to exit..." | Out-Null
